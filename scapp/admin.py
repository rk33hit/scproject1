from django.contrib import admin
from scapp.models import Student,Contact_Us,category1,register_t1,add_product,cart,Order
admin.site.site_header="rk33hit website | scproject"
# Register your models here.
class StudentAdmin(admin.ModelAdmin):
    # fields=["roll_no","name","email",]
    list_display =["name","roll_no","email","fee","gender","address","is_registered"]
    search_fields =["roll_no","name"]
    list_filter= ["name","gender"]
    list_editable =["email","address"]
class Contact_UsAdmin(admin.ModelAdmin):
    fields=["id","name","subjecct","contact_number","message"]
    list_display=["id","name","subjecct","contact_number","message","add_on"]

    search_fields=["contact_number","name"]
    list_filter=["name",'subjecct']

class category1Admin(admin.ModelAdmin):
    list_display = ["id","cat_name","desc","add_on"]
    
admin.site.register(Student,StudentAdmin)
admin.site.register(Contact_Us,Contact_UsAdmin)
admin.site.register(category1,category1Admin)
admin.site.register(register_t1)
admin.site.register(add_product)
admin.site.register(cart)
admin.site.register(Order)

